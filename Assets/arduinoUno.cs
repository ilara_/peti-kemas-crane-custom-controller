using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;

public class arduinoUno : MonoBehaviour
{
    [Header("Arduino Attribute")]
    [SerializeField] bool isConnected;
    [SerializeField] string portName;
    [SerializeField] int baudRate;
    [SerializeField] SerialPort data_stream;
    public List<string> joystickData;

    // Start is called before the first frame update
    void Start()
    {
        data_stream = new SerialPort(portName, baudRate);

        data_stream.Open();

        data_stream.ReadTimeout = 101;
    }

    // Update is called once per frame
    void Update()
    {
        if (data_stream.IsOpen)
        {
            try
            {
                string[] datas = data_stream.ReadLine().Split(',');

                joystickData[0] = datas[0];
                joystickData[1] = datas[1]; // 
                joystickData[2] = datas[2]; //
            }
            catch (System.Exception)
            {

            }
        }
        else
        {
            data_stream.Close();
            isConnected = false;
        }
    }
}
