using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controller : MonoBehaviour
{
    [Header("Arduino Manager")]
    [SerializeField] arduino arduinoo;

    [Header("Player Attribute")]
    [SerializeField] float runSpeed = 0.1f;
    [SerializeField] float xJoystick;
    [SerializeField] float yJoystick;
    [SerializeField] int pinJoystick;
    [SerializeField] int controlManager;
    [SerializeField] int controlMax;

    // Update is called once per frame
    void Update()
    {
        yJoystick = float.Parse(arduinoo.joystickData[1]);
        xJoystick = float.Parse(arduinoo.joystickData[0]);
        pinJoystick = int.Parse(arduinoo.joystickData[2]);

        if (pinJoystick != 0)
            controlManager++;

        if (controlManager == controlMax)
            controlManager = 0;

        switch (controlManager)
        {
            case 0:
                transform.Translate(new Vector3(xJoystick, 0, yJoystick) * (runSpeed * Time.deltaTime));
                break;

            case 1:
                transform.Translate(new Vector3(xJoystick, yJoystick, 0) * (runSpeed * Time.deltaTime));
                break;

            case 2:
                transform.Rotate(xJoystick, 0, yJoystick, Space.Self);
                transform.Rotate(xJoystick, 0, yJoystick, Space.World);
                break;
        }

        Debug.Log(controlManager);
    }
}
