using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class control : MonoBehaviour
{
    [Header("Arduino Manager")]
    [SerializeField] ArduinoManager arduinoo;

    [Header("Player Attribute")]
    [SerializeField] float runSpeed = 5;
    [SerializeField] float runSpeed2 = 3;
    [SerializeField] float RightYJoystick;
    [SerializeField] float RightXJoystick;
    [SerializeField] float LeftYJoystick;
    [SerializeField] float LeftXJoystick;
    [SerializeField] List<int> pushButtonDetail;

    public GameObject monitor;
    public GameObject craneTop;
    public GameObject crane;

    public bool capit;


    private void Start()
    {
        capit = false;
    }
    // Update is called once per frame
    void Update()
    {
        RightYJoystick = arduinoo.rightJoystickDetail.y;
        RightXJoystick = arduinoo.rightJoystickDetail.x;
        LeftYJoystick = arduinoo.leftJoystickDetail.y;
        LeftXJoystick = arduinoo.leftJoystickDetail.x;
        pushButtonDetail = arduinoo.pushButtonDetail;

        if (Input.GetKey(KeyCode.W) || LeftYJoystick >= 1)
        {
            monitor.transform.Translate(Vector3.forward * runSpeed * Time.deltaTime);
            Debug.Log("masuk");
        }
        if (Input.GetKey(KeyCode.A) || LeftXJoystick <= -1)
        {
            this.transform.Translate(Vector3.left * runSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S) || LeftYJoystick <= -1)
        {
            monitor.transform.Translate(-Vector3.forward * runSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D) || LeftXJoystick >= 1)
        {
            this.transform.Translate(Vector3.right * runSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.UpArrow) || RightYJoystick >= 1)
        {
            craneTop.transform.Translate(Vector3.up * runSpeed2 * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.DownArrow) || RightYJoystick <= -1)
        {
            craneTop.transform.Translate(-Vector3.up * runSpeed2 * Time.deltaTime);
        }
        if (Input.GetKeyDown(KeyCode.Space) || pushButtonDetail[0] == 1)
        {
            if (capit == true)
            {
                crane.transform.localScale += new Vector3(0f, 6f, 0f);
                capit = false;
            }
            else if(capit == false)
            {
                crane.transform.localScale -= new Vector3(0f, 6f, 0f);
                capit = true;
            }
        }
    }

}
